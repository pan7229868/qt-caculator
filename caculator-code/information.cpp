#include "information.h"
#include "ui_information.h"     
#include <QIcon>

Information::Information(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Information)
{
    ui->setupUi(this);
    setWindowIcon(QIcon(":/sdaulogo.ico"));
    ui->label_3->setPixmap(QPixmap(":/sdau.jpg"));
    ui->label_3->setScaledContents(true);
    ui->label_3->lower(); //！！！
    setWindowTitle("关于");
}

Information::~Information()
{
    delete ui;
}

void Information::on_pushButton_clicked()
{
    this->close();
}

